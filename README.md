# MoneyTransfers

How to start the MoneyTransfers application
---

1. Run `mvn clean package` to build the application
2. Start the application with `java -jar target/money-transfers-1.0-SNAPSHOT.jar server config.yml`
3. To check the application is running do: `curl -X GET http://localhost:8081/healthcheck`

API
---

######Get Accounts

`curl -X GET http://localhost:8080/accounts`

######Get Account

`curl -X GET http://localhost:8080/accounts/{id}`

######Create Account

`curl -X POST http://localhost:8080/accounts \
  -H 'Content-Type: application/json' \
  -d '{"currency": "EUR", "description": "current account"}'`

######Get Transactions

`curl -X GET http://localhost:8080/transactions`

######Get Transaction

`curl -X GET http://localhost:8080/transactions/1`

######Create Transaction

`curl -X POST http://localhost:8080/transactions \
  -H 'Content-Type: application/json' \
  -d '{"description": "dinner", "amount": 5, "srcAccountId": 1, "dstAccountId": 2}'`
