package com.revolut.moneytransfers;

import com.revolut.moneytransfers.exception.mapper.AccountNotFoundExceptionMapper;
import com.revolut.moneytransfers.exception.mapper.TransactionNotFoundExceptionMapper;
import com.revolut.moneytransfers.health.HealthCheck;
import com.revolut.moneytransfers.model.Account;
import com.revolut.moneytransfers.persistence.inmemory.AccountInMemoryDao;
import com.revolut.moneytransfers.persistence.inmemory.TransactionInMemoryDao;
import com.revolut.moneytransfers.resource.AccountsResource;
import com.revolut.moneytransfers.resource.TransactionsResource;
import com.revolut.moneytransfers.service.AccountService;
import com.revolut.moneytransfers.service.CurrencyConverterService;
import com.revolut.moneytransfers.service.TransactionService;
import com.revolut.moneytransfers.service.impl.AccountServiceImpl;
import com.revolut.moneytransfers.service.impl.FlatCurrencyConverterService;
import com.revolut.moneytransfers.service.impl.TransactionServiceImpl;
import io.dropwizard.setup.Environment;

import java.util.Currency;

public class Application extends io.dropwizard.Application<Configuration> {

    public static void main(final String[] args) throws Exception {
        new Application().run(args);
    }

    @Override
    public void run(final Configuration configuration,
                    final Environment environment) {
        AccountService accountService = new AccountServiceImpl(new AccountInMemoryDao());
        CurrencyConverterService currencyConverterService = new FlatCurrencyConverterService();
        TransactionService transactionService = new TransactionServiceImpl(new TransactionInMemoryDao(),
                accountService, currencyConverterService);

        environment.jersey().register(new AccountsResource(accountService));
        environment.jersey().register(new TransactionsResource(transactionService));
        environment.jersey().register(new AccountNotFoundExceptionMapper());
        environment.jersey().register(new TransactionNotFoundExceptionMapper());
        environment.healthChecks().register("healthcheck", new HealthCheck());

        if(configuration.isTesting()) {
            Account account = accountService.createAccount(new Account(Currency.getInstance("EUR"), "current account"));
            accountService.setAccountBalance(account.getId(), 10);
        }
    }

}
