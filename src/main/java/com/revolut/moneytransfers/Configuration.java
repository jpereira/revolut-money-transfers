package com.revolut.moneytransfers;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Configuration extends io.dropwizard.Configuration {

    private boolean testing;

    @JsonProperty("testing")
    public boolean isTesting() {
        return testing;
    }

}
