package com.revolut.moneytransfers.exception.mapper;

import com.revolut.moneytransfers.exception.TransactionNotFoundException;
import org.eclipse.jetty.http.HttpStatus;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class TransactionNotFoundExceptionMapper implements ExceptionMapper<TransactionNotFoundException> {

    @Override
    public Response toResponse(TransactionNotFoundException e) {
        return Response.status(HttpStatus.NOT_FOUND_404).entity(e).build();
    }

}
