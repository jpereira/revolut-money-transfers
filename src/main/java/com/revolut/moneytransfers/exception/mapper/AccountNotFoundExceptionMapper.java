package com.revolut.moneytransfers.exception.mapper;

import com.revolut.moneytransfers.exception.AccountNotFoundException;
import org.eclipse.jetty.http.HttpStatus;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class AccountNotFoundExceptionMapper implements ExceptionMapper<AccountNotFoundException> {

    @Override
    public Response toResponse(AccountNotFoundException e) {
        return Response.status(HttpStatus.NOT_FOUND_404).entity(e).build();
    }

}
