package com.revolut.moneytransfers.exception;

public class TransactionNotFoundException extends RuntimeException {

    public TransactionNotFoundException(String msg) {
        super(msg);
    }

}
