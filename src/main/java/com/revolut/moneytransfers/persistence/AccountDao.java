package com.revolut.moneytransfers.persistence;

import com.revolut.moneytransfers.model.Account;

import java.util.Collection;

public interface AccountDao {

    Account getAccount(long id);

    Collection<Account> getAccounts();

    Account createAccount(Account account);

    Account setAccountBalance(long id, long balance);

    Account addAccountBalance(long id, long amount);

}
