package com.revolut.moneytransfers.persistence.inmemory;

import com.revolut.moneytransfers.model.Account;
import com.revolut.moneytransfers.persistence.AccountDao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class AccountInMemoryDao implements AccountDao {

    private final AtomicLong ACCOUNT_ID_COUNTER = new AtomicLong();
    private final Map<Long, Account> ACCOUNTS = new HashMap<>();

    @Override
    public Account getAccount(long id) {
        return ACCOUNTS.get(id);
    }

    @Override
    public Collection<Account> getAccounts() {
        return ACCOUNTS.values();
    }

    @Override
    public Account createAccount(Account account) {
        account.setFinalId(ACCOUNT_ID_COUNTER.incrementAndGet());
        ACCOUNTS.put(account.getId(), account);
        return account;
    }

    @Override
    public Account setAccountBalance(long id, long balance) {
        Account account = ACCOUNTS.get(id);
        account.getBalance().set(balance);
        return account;
    }

    @Override
    public Account addAccountBalance(long id, long amount) {
        Account account = ACCOUNTS.get(id);
        account.getBalance().getAndAdd(amount);
        return account;
    }
}
