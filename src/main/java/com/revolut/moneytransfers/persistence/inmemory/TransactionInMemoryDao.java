package com.revolut.moneytransfers.persistence.inmemory;

import com.revolut.moneytransfers.model.Transaction;
import com.revolut.moneytransfers.persistence.TransactionDao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class TransactionInMemoryDao implements TransactionDao {

    private final AtomicLong TRANSACTION_ID_COUNTER = new AtomicLong();
    private final Map<Long, Transaction> TRANSACTIONS = new HashMap<>();

    @Override
    public Transaction getTransaction(long id) {
        return TRANSACTIONS.get(id);
    }

    @Override
    public Collection<Transaction> getTransactions() {
        return TRANSACTIONS.values();
    }

    @Override
    public Transaction createTransaction(Transaction transaction) {
        transaction.setFinalId(TRANSACTION_ID_COUNTER.incrementAndGet());
        TRANSACTIONS.put(transaction.getId(), transaction);
        return transaction;
    }

}
