package com.revolut.moneytransfers.persistence;

import com.revolut.moneytransfers.model.Transaction;

import java.util.Collection;

public interface TransactionDao {

    Transaction getTransaction(long id);

    Collection<Transaction> getTransactions();

    Transaction createTransaction(Transaction transaction);

}
