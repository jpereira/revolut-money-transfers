package com.revolut.moneytransfers.model;

public enum TransactionState {
    COMPLETED,
    DECLINED
}
