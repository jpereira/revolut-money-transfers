package com.revolut.moneytransfers.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import java.time.Instant;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Transaction {

    @Min(value = 1)
    private final long srcAccountId;

    @Min(value = 1)
    private final long dstAccountId;

    @Min(value = 1)
    private final long amount;

    @NotEmpty
    private final String created;

    private final String description;

    private long id;
    private TransactionState state;
    private String reason;

    @JsonCreator
    public Transaction(@JsonProperty("srcAccountId") long srcAccountId, @JsonProperty("dstAccountId") long dstAccountId,
                       @JsonProperty("amount") long amount, @JsonProperty("description") String description) {
        this.srcAccountId = srcAccountId;
        this.dstAccountId = dstAccountId;
        this.amount = amount;
        this.description = description;
        created = Instant.now().toString();
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("srcAccountId")
    public long getSrcAccountId() {
        return srcAccountId;
    }

    @JsonProperty("dstAccountId")
    public long getDstAccountId() {
        return dstAccountId;
    }

    @JsonProperty("amount")
    public long getAmount() {
        return amount;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("state")
    public TransactionState getState() {
        return state;
    }

    @JsonProperty("reason")
    public String getReason() {
        return reason;
    }

    public void setFinalId(long id) {
        this.id = this.id == 0 ? id : this.id;
    }

    public void setState(TransactionState state) {
        this.state = state;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return getId() == that.getId() &&
                getSrcAccountId() == that.getSrcAccountId() &&
                getDstAccountId() == that.getDstAccountId() &&
                getAmount() == that.getAmount() &&
                Objects.equals(getCreated(), that.getCreated()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                getState() == that.getState() &&
                Objects.equals(getReason(), that.getReason());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCreated(), getId(), getSrcAccountId(), getDstAccountId(), getAmount(), getDescription(), getState(), getReason());
    }

}
