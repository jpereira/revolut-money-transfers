package com.revolut.moneytransfers.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Currency;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class Account {

    @NotNull
    private final Currency currency;

    @NotEmpty
    private final String created;

    private final String description;

    private final AtomicLong balance;

    private long id;
    private String updated;

    @JsonCreator
    public Account(@JsonProperty("currency") Currency currency, @JsonProperty("description") String description) {
        this.currency = currency;
        this.description = description;
        created = updated = Instant.now().toString();
        balance = new AtomicLong(0);
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("currency")
    public Currency getCurrency() {
        return currency;
    }

    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("updated")
    public String getUpdated() {
        return updated;
    }

    @JsonProperty("balance")
    public AtomicLong getBalance() {
        return balance;
    }

    public void setFinalId(long id) {
        this.id = this.id == 0 ? id : this.id;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return getId() == account.getId() &&
                Objects.equals(getCurrency(), account.getCurrency()) &&
                Objects.equals(getCreated(), account.getCreated()) &&
                Objects.equals(getDescription(), account.getDescription()) &&
                Objects.equals(getUpdated(), account.getUpdated()) &&
                Objects.equals(getBalance().get(), account.getBalance().get());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCurrency(), getCreated(), getDescription(), getId(), getUpdated(), getBalance().get());
    }

}
