package com.revolut.moneytransfers.service.impl;

import com.revolut.moneytransfers.service.CurrencyConverterService;

import java.util.Currency;

public class FlatCurrencyConverterService implements CurrencyConverterService {

    @Override
    public long convertAmount(Currency srcCurrency, long srcAmount, Currency dstCurrency) {
        return srcAmount;
    }

}
