package com.revolut.moneytransfers.service.impl;

import com.revolut.moneytransfers.exception.AccountNotFoundException;
import com.revolut.moneytransfers.exception.TransactionNotFoundException;
import com.revolut.moneytransfers.model.Account;
import com.revolut.moneytransfers.model.Transaction;
import com.revolut.moneytransfers.model.TransactionState;
import com.revolut.moneytransfers.persistence.TransactionDao;
import com.revolut.moneytransfers.service.AccountService;
import com.revolut.moneytransfers.service.CurrencyConverterService;
import com.revolut.moneytransfers.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.Collection;

public class TransactionServiceImpl implements TransactionService {

    private final Logger log = LoggerFactory.getLogger(TransactionServiceImpl.class);
    private final TransactionDao transactionDao;
    private final AccountService accountService;
    private final CurrencyConverterService currencyConverterService;

    public TransactionServiceImpl(TransactionDao transactionDao, AccountService accountService,
                                  CurrencyConverterService currencyConverterService) {
        this.transactionDao = transactionDao;
        this.accountService = accountService;
        this.currencyConverterService = currencyConverterService;
    }

    @Override
    public Transaction getTransaction(long id) throws TransactionNotFoundException {
        Transaction transaction = transactionDao.getTransaction(id);

        if(transaction == null) {
            String msg = String.format("Transaction with id=%d not found", id);
            TransactionNotFoundException exception = new TransactionNotFoundException(msg);
            log.info(msg, exception);
            throw exception;
        } else {
            return transaction;
        }
    }

    @Override
    public Collection<Transaction> getTransactions() {
        return transactionDao.getTransactions();
    }

    @Override
    public Transaction createTransaction(Transaction transaction) throws AccountNotFoundException {
        Account srcAccount = accountService.getAccount(transaction.getSrcAccountId());
        Account dstAccount = accountService.getAccount(transaction.getDstAccountId());
        boolean srcAccountHasFunds = false;

        synchronized (srcAccount) {
            if(srcAccount.getBalance().get() >= transaction.getAmount()) {
                srcAccount = accountService.setAccountBalance(srcAccount.getId(),
                        srcAccount.getBalance().get() - transaction.getAmount());
                srcAccountHasFunds = true;
            }
        }

        if(srcAccountHasFunds) {
            srcAccount.setUpdated(Instant.now().toString());

            if(!srcAccount.getCurrency().equals(dstAccount.getCurrency())) {
                long convertedAmount = currencyConverterService.convertAmount(srcAccount.getCurrency(),
                        transaction.getAmount(), dstAccount.getCurrency());
                accountService.addAccountBalance(dstAccount.getId(), convertedAmount);
            } else {
                accountService.addAccountBalance(dstAccount.getId(), transaction.getAmount());
            }

            dstAccount.setUpdated(Instant.now().toString());
            transaction.setState(TransactionState.COMPLETED);
        } else {
            transaction.setState(TransactionState.DECLINED);
            transaction.setReason("Insufficient funds");
        }

        Transaction transactionCreated = transactionDao.createTransaction(transaction);
        log.info(String.format("Transaction created with id=%d and state=%s",
                transactionCreated.getId(), transactionCreated.getState()));
        return transactionCreated;
    }

}
