package com.revolut.moneytransfers.service.impl;

import com.revolut.moneytransfers.exception.AccountNotFoundException;
import com.revolut.moneytransfers.model.Account;
import com.revolut.moneytransfers.persistence.AccountDao;
import com.revolut.moneytransfers.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public class AccountServiceImpl implements AccountService {

    private final Logger log = LoggerFactory.getLogger(AccountServiceImpl.class);
    private final AccountDao accountDao;

    public AccountServiceImpl(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public Account getAccount(long id) throws AccountNotFoundException {
        Account account = accountDao.getAccount(id);

        if(account == null) {
            String msg = String.format("Account with id=%d not found", id);
            AccountNotFoundException exception = new AccountNotFoundException(msg);
            log.info(msg, exception);
            throw exception;
        } else {
            return account;
        }
    }

    @Override
    public Collection<Account> getAccounts() {
        return accountDao.getAccounts();
    }

    @Override
    public Account createAccount(Account account) {
        Account accountCreated = accountDao.createAccount(account);
        log.info(String.format("Account created with id=%d", accountCreated.getId()));
        return accountCreated;
    }

    @Override
    public Account setAccountBalance(long id, long balance) {
        Account account = accountDao.setAccountBalance(id, balance);
        log.info(String.format("Account with id=%d set balance=%d", account.getId(), account.getBalance().get()));
        return account;
    }

    @Override
    public Account addAccountBalance(long id, long amount) {
        Account account = accountDao.addAccountBalance(id, amount);
        log.info(String.format("Account with id=%d add balance=%d", account.getId(), amount));
        return account;
    }

}
