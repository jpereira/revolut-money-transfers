package com.revolut.moneytransfers.service;

import com.revolut.moneytransfers.exception.AccountNotFoundException;
import com.revolut.moneytransfers.model.Account;

import java.util.Collection;

public interface AccountService {

    Account getAccount(long id) throws AccountNotFoundException;

    Collection<Account> getAccounts();

    Account createAccount(Account account);

    Account setAccountBalance(long id, long balance);

    Account addAccountBalance(long id, long amount);
}
