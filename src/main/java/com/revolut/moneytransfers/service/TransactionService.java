package com.revolut.moneytransfers.service;

import com.revolut.moneytransfers.exception.AccountNotFoundException;
import com.revolut.moneytransfers.exception.TransactionNotFoundException;
import com.revolut.moneytransfers.model.Transaction;

import java.util.Collection;

public interface TransactionService {

    Transaction getTransaction(long id) throws TransactionNotFoundException;

    Collection<Transaction> getTransactions();

    Transaction createTransaction(Transaction transaction) throws AccountNotFoundException;

}
