package com.revolut.moneytransfers.service;

import java.util.Currency;

public interface CurrencyConverterService {

    long convertAmount(Currency srcCurrency, long srcAmount, Currency dstCurrency);

}
