package com.revolut.moneytransfers.resource;

import com.revolut.moneytransfers.model.Transaction;
import com.revolut.moneytransfers.service.TransactionService;
import org.eclipse.jetty.http.HttpStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path(TransactionsResource.PATH)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TransactionsResource {

    static final String PATH = "/transactions";

    private final TransactionService transactionService;

    public TransactionsResource(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GET
    @Path("/{id}")
    public Transaction getTransaction(@PathParam("id") @NotNull Long id) {
        return transactionService.getTransaction(id);
    }

    @GET
    public Collection<Transaction> getTransactions() {
        return transactionService.getTransactions();
    }

    @POST
    public Response createTransaction(@Valid Transaction transaction) {
        Transaction createTransaction = transactionService.createTransaction(transaction);
        return Response.status(HttpStatus.CREATED_201).entity(createTransaction).build();
    }

}
