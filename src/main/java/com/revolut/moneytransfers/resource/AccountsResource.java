package com.revolut.moneytransfers.resource;

import com.revolut.moneytransfers.model.Account;
import com.revolut.moneytransfers.service.AccountService;
import org.eclipse.jetty.http.HttpStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path(AccountsResource.PATH)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AccountsResource {

    static final String PATH = "/accounts";

    private final AccountService accountService;

    public AccountsResource(AccountService accountService) {
        this.accountService = accountService;
    }

    @GET
    @Path("/{id}")
    public Account getAccount(@PathParam("id") @NotNull Long id) {
        return accountService.getAccount(id);
    }

    @GET
    public Collection<Account> getAccounts() {
        return accountService.getAccounts();
    }

    @POST
    public Response createAccount(@Valid Account account) {
        Account createdAccount = accountService.createAccount(account);
        return Response.status(HttpStatus.CREATED_201).entity(createdAccount).build();
    }

}
