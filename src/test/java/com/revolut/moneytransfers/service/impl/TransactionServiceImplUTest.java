package com.revolut.moneytransfers.service.impl;

import com.revolut.moneytransfers.exception.AccountNotFoundException;
import com.revolut.moneytransfers.exception.TransactionNotFoundException;
import com.revolut.moneytransfers.model.Account;
import com.revolut.moneytransfers.model.Transaction;
import com.revolut.moneytransfers.model.TransactionState;
import com.revolut.moneytransfers.persistence.TransactionDao;
import com.revolut.moneytransfers.service.AccountService;
import com.revolut.moneytransfers.service.CurrencyConverterService;
import com.revolut.moneytransfers.service.TransactionService;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Currency;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TransactionServiceImplUTest {

    private final TransactionDao transactionDao = mock(TransactionDao.class);
    private final AccountService accountService = mock(AccountService.class);
    private final CurrencyConverterService currencyConverterService = mock(CurrencyConverterService.class);
    private final TransactionService transactionService =
            new TransactionServiceImpl(transactionDao, accountService, currencyConverterService);
    private final Transaction transaction = new Transaction(1, 2, 1, null);

    @Test
    public void testGetTransaction() {
        when(transactionDao.getTransaction(1)).thenReturn(transaction);

        Transaction retTransaction = transactionService.getTransaction(1);

        assertThat(retTransaction).isEqualTo(transaction);
        verify(transactionDao).getTransaction(1);
    }

    @Test(expected = TransactionNotFoundException.class)
    public void testGetTransactionNotFound() {
        when(transactionDao.getTransaction(1)).thenReturn(null);

        transactionService.getTransaction(1);
    }

    @Test
    public void testGetTransactions() {
        Transaction anotherTransaction = new Transaction(2, 1, 1, null);
        when(transactionDao.getTransactions()).thenReturn(Arrays.asList(transaction, anotherTransaction));

        Collection<Transaction> retTransactions = transactionService.getTransactions();

        assertThat(retTransactions).containsExactly(transaction, anotherTransaction);
        verify(transactionDao).getTransactions();
    }

    @Test(expected = AccountNotFoundException.class)
    public void testCreateTransactionWithNonExistingAccount() {
        when(accountService.getAccount(any(Long.class))).thenThrow(AccountNotFoundException.class);

        transactionService.createTransaction(transaction);
    }

    @Test
    public void testCreateTransactionCompleted() {
        Account account1 = new Account(Currency.getInstance("EUR"), null);
        Account account2 = new Account(Currency.getInstance("EUR"), null);
        Account updatedAccount1 = new Account(Currency.getInstance("EUR"), null);
        Account updatedAccount2 = new Account(Currency.getInstance("EUR"), null);
        account1.getBalance().set(1);
        updatedAccount2.getBalance().set(1);

        when(accountService.getAccount(1)).thenReturn(account1);
        when(accountService.getAccount(2)).thenReturn(account2);
        when(accountService.setAccountBalance(account1.getId(),
                account1.getBalance().get() - transaction.getAmount())).thenReturn(updatedAccount1);
        when(accountService.addAccountBalance(account2.getId(), transaction.getAmount())).thenReturn(updatedAccount2);

        when(transactionDao.createTransaction(transaction)).thenReturn(transaction);

        Transaction retTransaction = transactionService.createTransaction(transaction);

        verify(transactionDao).createTransaction(transaction);
        assertThat(retTransaction.getState()).isEqualTo(TransactionState.COMPLETED);
        assertThat(updatedAccount1.getBalance().get()).isZero();
        assertThat(updatedAccount2.getBalance().get()).isOne();
    }

    @Test
    public void testCreateTransactionDeclined() {
        Account account1 = new Account(Currency.getInstance("EUR"), null);
        Account account2 = new Account(Currency.getInstance("EUR"), null);

        when(accountService.getAccount(1)).thenReturn(account1);
        when(accountService.getAccount(2)).thenReturn(account2);

        when(transactionDao.createTransaction(transaction)).thenReturn(transaction);

        Transaction retTransaction = transactionService.createTransaction(transaction);

        verify(transactionDao).createTransaction(transaction);
        assertThat(retTransaction.getState()).isEqualTo(TransactionState.DECLINED);
        assertThat(account1.getBalance().get()).isZero();
        assertThat(account2.getBalance().get()).isZero();
        assertThat(account1.getUpdated()).isEqualTo(account1.getCreated());
        assertThat(account2.getUpdated()).isEqualTo(account2.getCreated());
    }

    @Test
    public void testCreateTransactionWithDifferentCurrencies() {
        Account account1 = new Account(Currency.getInstance("EUR"), null);
        Account account2 = new Account(Currency.getInstance("GBP"), null);
        Account updatedAccount1 = new Account(Currency.getInstance("EUR"), null);
        Account updatedAccount2 = new Account(Currency.getInstance("GBP"), null);
        account1.getBalance().set(1);
        updatedAccount2.getBalance().set(1);

        when(accountService.getAccount(1)).thenReturn(account1);
        when(accountService.getAccount(2)).thenReturn(account2);
        when(accountService.setAccountBalance(account1.getId(),
                account1.getBalance().get() - transaction.getAmount())).thenReturn(updatedAccount1);
        when(currencyConverterService.convertAmount(Currency.getInstance("EUR"), transaction.getAmount(),
                Currency.getInstance("GBP"))).thenReturn(1L);
        when(accountService.addAccountBalance(account2.getId(), 1L)).thenReturn(updatedAccount2);

        when(transactionDao.createTransaction(transaction)).thenReturn(transaction);

        Transaction retTransaction = transactionService.createTransaction(transaction);

        verify(currencyConverterService).convertAmount(account1.getCurrency(), transaction.getAmount(),
                account2.getCurrency());
        verify(transactionDao).createTransaction(transaction);
        assertThat(retTransaction.getState()).isEqualTo(TransactionState.COMPLETED);
        assertThat(updatedAccount1.getBalance().get()).isZero();
        assertThat(updatedAccount2.getBalance().get()).isOne();
    }

}
