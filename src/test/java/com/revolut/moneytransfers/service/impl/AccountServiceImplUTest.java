package com.revolut.moneytransfers.service.impl;

import com.revolut.moneytransfers.exception.AccountNotFoundException;
import com.revolut.moneytransfers.model.Account;
import com.revolut.moneytransfers.persistence.AccountDao;
import com.revolut.moneytransfers.service.AccountService;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Currency;
import java.util.concurrent.atomic.AtomicLong;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AccountServiceImplUTest {

    private final AccountDao accountDao = mock(AccountDao.class);
    private final AccountService accountService = new AccountServiceImpl(accountDao);
    private final Account account = new Account(Currency.getInstance("EUR"), "current account");

    @Test
    public void testGetAccount() {
        when(accountDao.getAccount(1)).thenReturn(account);

        Account retAccount = accountService.getAccount(1);

        assertThat(retAccount).isEqualTo(account);
        verify(accountDao).getAccount(1);
    }

    @Test(expected = AccountNotFoundException.class)
    public void testGetAccountNotFound() {
        when(accountDao.getAccount(1)).thenReturn(null);

        accountService.getAccount(1);
    }

    @Test
    public void testGetAccounts() {
        Account anotherAccount = new Account(Currency.getInstance("EUR"), null);
        when(accountDao.getAccounts()).thenReturn(Arrays.asList(account, anotherAccount));

        Collection<Account> retAccounts = accountService.getAccounts();

        assertThat(retAccounts).containsExactly(account, anotherAccount);
        verify(accountDao).getAccounts();
    }

    @Test
    public void testCreateAccount() {
        when(accountDao.createAccount(account)).thenReturn(account);

        Account retAccount = accountService.createAccount(account);

        assertThat(retAccount).isEqualTo(account);
        verify(accountDao).createAccount(account);
    }

    @Test
    public void testSetAccountBalance() {
        Account createdAccount = mock(Account.class);
        when(createdAccount.getId()).thenReturn(1L);
        when(createdAccount.getBalance()).thenReturn(new AtomicLong(1));
        when(accountDao.setAccountBalance(createdAccount.getId(), createdAccount.getBalance().get()))
                .thenReturn(createdAccount);

        Account retAccount = accountService.setAccountBalance(createdAccount.getId(), createdAccount.getBalance().get());

        assertThat(retAccount.getBalance()).isEqualTo(createdAccount.getBalance());
        verify(accountDao).setAccountBalance(createdAccount.getId(), createdAccount.getBalance().get());
    }

    @Test
    public void testAddAccountBalance() {
        Account createdAccount = mock(Account.class);
        when(createdAccount.getId()).thenReturn(1L);
        when(createdAccount.getBalance()).thenReturn(new AtomicLong(1));
        when(accountDao.addAccountBalance(createdAccount.getId(), createdAccount.getBalance().get()))
                .thenReturn(createdAccount);

        Account retAccount = accountService.addAccountBalance(createdAccount.getId(), createdAccount.getBalance().get());

        assertThat(retAccount.getBalance().get()).isEqualTo(createdAccount.getBalance().get());
        verify(accountDao).addAccountBalance(createdAccount.getId(), createdAccount.getBalance().get());
    }

}
