package com.revolut.moneytransfers.persistence.inmemory;

import com.revolut.moneytransfers.model.Transaction;
import com.revolut.moneytransfers.persistence.TransactionDao;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

public class TransactionInMemoryDaoUTest {

    private TransactionDao transactionDao;

    @Before
    public void before() {
        transactionDao = new TransactionInMemoryDao();
    }

    @Test
    public void testGetTransaction() {
        Transaction transaction = createTransaction();
        Transaction existingTransaction = transactionDao.createTransaction(transaction);

        Transaction retTransaction = transactionDao.getTransaction(existingTransaction.getId());

        assertThat(retTransaction).isEqualTo(existingTransaction);
    }

    @Test
    public void testGetTransactionNotFound() {
        Transaction retTransaction = transactionDao.getTransaction(1);

        assertThat(retTransaction).isNull();
    }

    @Test
    public void testGetTransactions() {
        Transaction transaction = createTransaction();
        Transaction anotherTransaction = createTransaction();

        Transaction existingTransaction = transactionDao.createTransaction(transaction);
        Transaction anotherExistingTransaction = transactionDao.createTransaction(anotherTransaction);

        Collection<Transaction> retTransactions = transactionDao.getTransactions();

        assertThat(retTransactions).containsExactly(existingTransaction, anotherExistingTransaction);
    }

    @Test
    public void testCreateTransaction() {
        Transaction transaction = createTransaction();

        Transaction retTransaction = transactionDao.createTransaction(transaction);

        assertThat(retTransaction).isEqualTo(transaction);
    }

    private Transaction createTransaction() {
        return new Transaction(1, 2, 1, null);
    }

}
