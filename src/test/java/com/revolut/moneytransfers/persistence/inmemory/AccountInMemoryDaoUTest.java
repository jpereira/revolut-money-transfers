package com.revolut.moneytransfers.persistence.inmemory;

import com.revolut.moneytransfers.model.Account;
import com.revolut.moneytransfers.persistence.AccountDao;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Currency;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountInMemoryDaoUTest {

    private AccountDao accountDao;

    @Before
    public void before() {
        accountDao = new AccountInMemoryDao();
    }

    @Test
    public void testGetAccount() {
        Account account = getNewAccount();
        Account existingAccount = accountDao.createAccount(account);

        Account retAccount = accountDao.getAccount(existingAccount.getId());

        assertThat(retAccount).isEqualTo(existingAccount);
    }

    @Test
    public void testGetAccountNotFound() {
        Account retAccount = accountDao.getAccount(1);

        assertThat(retAccount).isNull();
    }

    @Test
    public void testGetAccounts() {
        Account account = getNewAccount();
        Account anotherAccount = getNewAccount();

        Account existingAccount = accountDao.createAccount(account);
        Account anotherExistingAccount = accountDao.createAccount(anotherAccount);

        Collection<Account> retAccounts = accountDao.getAccounts();

        assertThat(retAccounts).containsExactly(existingAccount, anotherExistingAccount);
    }

    @Test
    public void testCreateAccount() {
        Account account = getNewAccount();

        Account retAccount = accountDao.createAccount(account);

        assertThat(retAccount).isEqualTo(account);
    }

    @Test
    public void testSetAccountBalance() {
        Account account = accountDao.createAccount(getNewAccount());

        Account retAccount = accountDao.setAccountBalance(account.getId(), 1);

        assertThat(retAccount.getBalance().get()).isEqualTo(1);
    }

    @Test
    public void testAddAccountBalance() {
        Account account = accountDao.createAccount(getNewAccount());
        accountDao.setAccountBalance(account.getId(), 1);

        Account retAccount = accountDao.addAccountBalance(account.getId(), 1);

        assertThat(retAccount.getBalance().get()).isEqualTo(2);
    }

    private Account getNewAccount() {
        return new Account(Currency.getInstance("EUR"), "current account");
    }

}
