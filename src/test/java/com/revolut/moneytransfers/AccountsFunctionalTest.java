package com.revolut.moneytransfers;

import com.revolut.moneytransfers.model.Account;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Rule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.Currency;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.assertj.core.api.Assertions.assertThat;

public class AccountsFunctionalTest {

    @Rule
    public final DropwizardAppRule<Configuration> APP_RULE = new DropwizardAppRule<>(Application.class);

    private final Account account = new Account(Currency.getInstance("EUR"), "current account");

    @Test
    public void testGetAccount() {
        long id = createAccount();

        Response response = APP_RULE.client().target(
                String.format("http://localhost:%d/accounts/" + id, APP_RULE.getLocalPort()))
                .request()
                .get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK_200);
        assertJsonEquals(fixture("fixtures/account.json"), response.readEntity(String.class));
    }

    @Test
    public void testGetAccountNotFound() {
        Response response = APP_RULE.client().target(
                String.format("http://localhost:%d/accounts/1", APP_RULE.getLocalPort()))
                .request()
                .get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND_404);
    }

    @Test
    public void testGetAccounts() {
        createAccount();
        createAccount();

        Response response = APP_RULE.client().target(
                String.format("http://localhost:%d/accounts", APP_RULE.getLocalPort()))
                .request()
                .get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK_200);
        assertJsonEquals(fixture("fixtures/accounts.json"), response.readEntity(String.class));
    }

    @Test
    public void testCreateAccount() {
        Response response = APP_RULE.client().target(
                String.format("http://localhost:%d/accounts", APP_RULE.getLocalPort()))
                .request()
                .post(Entity.json(account));

        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED_201);
        assertJsonEquals(fixture("fixtures/account.json"), response.readEntity(String.class));
    }

    private long createAccount() {
        Account retAccount = APP_RULE.client().target(
                String.format("http://localhost:%d/accounts", APP_RULE.getLocalPort()))
                .request()
                .post(Entity.json(this.account)).readEntity(Account.class);

        return retAccount.getId();
    }

}
