package com.revolut.moneytransfers;

import com.revolut.moneytransfers.model.Account;
import com.revolut.moneytransfers.model.Transaction;
import io.dropwizard.testing.ConfigOverride;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.Rule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.Currency;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.assertj.core.api.Assertions.assertThat;

public class TransactionsFunctionalTest {

    @Rule
    public final DropwizardAppRule<Configuration> APP_RULE =
            new DropwizardAppRule<>(Application.class, null, ConfigOverride.config("testing", "true"));

    private final Account account = new Account(Currency.getInstance("EUR"), "current account");
    private final Transaction transaction = new Transaction(1, 2, 100,"dinner");

    @Test
    public void testGetTransaction() {
        createAccount();
        long id = createTransaction();

        Response response = APP_RULE.client().target(
                String.format("http://localhost:%d/transactions/" + id, APP_RULE.getLocalPort()))
                .request()
                .get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK_200);
        assertJsonEquals(fixture("fixtures/transaction_declined.json"), response.readEntity(String.class));
    }

    @Test
    public void testGetTransactionNotFound() {
        Response response = APP_RULE.client().target(
                String.format("http://localhost:%d/transactions/1", APP_RULE.getLocalPort()))
                .request()
                .get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND_404);
    }

    @Test
    public void testGetTransactions() {
        createAccount();
        createTransaction();
        createTransaction();

        Response response = APP_RULE.client().target(
                String.format("http://localhost:%d/transactions/", APP_RULE.getLocalPort()))
                .request()
                .get();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK_200);
        assertJsonEquals(fixture("fixtures/transactions.json"), response.readEntity(String.class));
    }

    @Test
    public void testCreateTransactionDeclined() {
        createAccount();

        Response response = APP_RULE.client().target(
                String.format("http://localhost:%d/transactions", APP_RULE.getLocalPort()))
                .request()
                .post(Entity.json(transaction));

        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED_201);
        assertJsonEquals(fixture("fixtures/transaction_declined.json"), response.readEntity(String.class));
    }

    @Test
    public void testCreateTransactionCompleted() {
        createAccount();
        Transaction transaction = new Transaction(1, 2, 10, "dinner");

        Response response = APP_RULE.client().target(
                String.format("http://localhost:%d/transactions", APP_RULE.getLocalPort()))
                .request()
                .post(Entity.json(transaction));

        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED_201);
        assertJsonEquals(fixture("fixtures/transaction_completed.json"), response.readEntity(String.class));
    }

    @Test
    public void testCreateTransactionCompletedWithDifferentCurrencies() {
        Account accountGbp = new Account(Currency.getInstance("GBP"), "current account");

        APP_RULE.client().target(
                String.format("http://localhost:%d/accounts", APP_RULE.getLocalPort()))
                .request()
                .post(Entity.json(accountGbp));

        Transaction transaction = new Transaction(1, 2, 10, "dinner");

        Response response = APP_RULE.client().target(
                String.format("http://localhost:%d/transactions", APP_RULE.getLocalPort()))
                .request()
                .post(Entity.json(transaction));

        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED_201);
        assertJsonEquals(fixture("fixtures/transaction_completed.json"), response.readEntity(String.class));
    }

    private void createAccount() {
        APP_RULE.client().target(
                String.format("http://localhost:%d/accounts", APP_RULE.getLocalPort()))
                .request()
                .post(Entity.json(account));
    }

    private long createTransaction() {
        Transaction retTransaction = APP_RULE.client().target(
                String.format("http://localhost:%d/transactions", APP_RULE.getLocalPort()))
                .request()
                .post(Entity.json(transaction)).readEntity(Transaction.class);

        return retTransaction.getId();
    }

}
