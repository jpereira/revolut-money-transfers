package com.revolut.moneytransfers.model;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TransactionUTest {

    @Test
    public void testSetFinalId() {
        Transaction transaction = new Transaction(0, 0, 0, null);

        transaction.setFinalId(1);

        assertThat(transaction.getId()).isEqualTo(1);
    }

    @Test
    public void testSetIdFinalImmutability() {
        Transaction transaction = new Transaction(0, 0, 0, null);

        transaction.setFinalId(1);
        transaction.setFinalId(2);

        assertThat(transaction.getId()).isEqualTo(1);
    }

}
