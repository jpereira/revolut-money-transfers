package com.revolut.moneytransfers.model;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountUTest {

    @Test
    public void testSetFinalId() {
        Account account = new Account(null, null);

        account.setFinalId(1);

        assertThat(account.getId()).isEqualTo(1);
    }

    @Test
    public void testSetFinalIdImmutability() {
        Account account = new Account(null, null);

        account.setFinalId(1);
        account.setFinalId(2);

        assertThat(account.getId()).isEqualTo(1);
    }

}
