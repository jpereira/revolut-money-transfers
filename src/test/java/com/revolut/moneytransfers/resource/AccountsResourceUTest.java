package com.revolut.moneytransfers.resource;

import com.revolut.moneytransfers.model.Account;
import com.revolut.moneytransfers.service.AccountService;
import io.dropwizard.jersey.validation.ValidationErrorMessage;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collection;
import java.util.Currency;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AccountsResourceUTest {

    private static final AccountService accountService = mock(AccountService.class);
    private static final AccountsResource accountsResource = new AccountsResource(accountService);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(accountsResource)
            .build();

    private final Account account = new Account(Currency.getInstance("EUR"), "current account");

    @Test
    public void testGetAccount() {
        when(accountService.getAccount(1)).thenReturn(account);

        final Response get = resources.target(AccountsResource.PATH + "/1").request().get();

        assertThat(get.getStatus()).isEqualTo(HttpStatus.OK_200);
        assertThat(get.readEntity(Account.class)).isEqualTo(account);
        verify(accountService).getAccount(1);
    }

    @Test
    public void testGetAccounts() {
        Account anotherAccount = new Account(Currency.getInstance("EUR"), null);
        when(accountService.getAccounts()).thenReturn(Arrays.asList(account, anotherAccount));

        Collection<Account> retAccounts = accountsResource.getAccounts();

        assertThat(retAccounts).containsExactly(account, anotherAccount);
        verify(accountService).getAccounts();
    }

    @Test
    public void testCreateAccount() {
        when(accountService.createAccount(account)).thenReturn(account);

        final Response post = resources.target(AccountsResource.PATH).request()
                .post(Entity.json(account));

        assertThat(post.getStatus()).isEqualTo(HttpStatus.CREATED_201);
        assertThat(post.readEntity(Account.class)).isEqualTo(account);
        verify(accountService).createAccount(account);
    }

    @Test
    public void testAccountCurrencyIsMandatory() {
        final Response post = resources.target(AccountsResource.PATH).request()
                .post(Entity.json(new Account(null, null)));

        assertThat(post.getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY_422);
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("currency may not be null");
    }

}
