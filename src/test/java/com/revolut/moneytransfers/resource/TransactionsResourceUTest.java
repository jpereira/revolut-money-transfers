package com.revolut.moneytransfers.resource;

import com.revolut.moneytransfers.model.Transaction;
import com.revolut.moneytransfers.service.TransactionService;
import io.dropwizard.jersey.validation.ValidationErrorMessage;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TransactionsResourceUTest {

    private static final TransactionService transactionService = mock(TransactionService.class);
    private static final TransactionsResource transactionsResource = new TransactionsResource(transactionService);

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder()
            .addResource(transactionsResource)
            .build();

    private final Transaction transaction = new Transaction(1, 2, 1, "dinner");

    @Test
    public void testGetTransaction() {
        when(transactionService.getTransaction(1)).thenReturn(transaction);

        final Response get = resources.target(TransactionsResource.PATH + "/1").request().get();

        assertThat(get.getStatus()).isEqualTo(HttpStatus.OK_200);
        assertThat(get.readEntity(Transaction.class)).isEqualTo(transaction);
        verify(transactionService).getTransaction(1);
    }

    @Test
    public void testGetTransactions() {
        Transaction anotherTransaction = new Transaction(2, 1, 1, null);
        when(transactionService.getTransactions()).thenReturn(Arrays.asList(transaction, anotherTransaction));

        Collection<Transaction> retTransactions = transactionsResource.getTransactions();

        assertThat(retTransactions).containsExactly(transaction, anotherTransaction);
        verify(transactionService).getTransactions();
    }

    @Test
    public void testCreateTransaction() {
        when(transactionService.createTransaction(transaction)).thenReturn(transaction);

        final Response post = resources.target(TransactionsResource.PATH).request()
                .post(Entity.json(transaction));

        assertThat(post.getStatus()).isEqualTo(HttpStatus.CREATED_201);
        assertThat(post.readEntity(Transaction.class)).isEqualTo(transaction);
        verify(transactionService).createTransaction(transaction);
    }

    @Test
    public void testTransactionSrcAccountIsMandatory() {
        final Response post = resources.target(TransactionsResource.PATH).request()
                .post(Entity.json(new Transaction(0, 2, 1, null)));

        assertThat(post.getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY_422);
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("srcAccountId must be greater than or equal to 1");
    }

    @Test
    public void testTransactionDstAccountIsMandatory() {
        final Response post = resources.target(TransactionsResource.PATH).request()
                .post(Entity.json(new Transaction(1, 0, 1, null)));

        assertThat(post.getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY_422);
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("dstAccountId must be greater than or equal to 1");
    }

    @Test
    public void testTransactionAmountIsMandatory() {
        final Response post = resources.target(TransactionsResource.PATH).request()
                .post(Entity.json(new Transaction(1, 2, 0, null)));

        assertThat(post.getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY_422);
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("amount must be greater than or equal to 1");
    }

}
